FROM elixir:latest

RUN apt-get update \
#  && apt-get install -y postgresql-client \
 && mix local.hex --force \
 && mix archive.install --force hex phx_new \
 && curl -sL https://deb.nodesource.com/setup_{NODE_VERSION} | bash -\
 && apt-get install -y git nodejs inotify-tools apt-utils build-essential \
 && apt-get install -y chromium-driver \
 && mix local.rebar --force

ENV APP_HOME /opt/app

RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME

EXPOSE 4000

CMD ["mix", "phx.server"]