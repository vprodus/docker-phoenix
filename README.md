# Phoenix Development

To start new Phoenix proejct new_project in ~/project folder:

```git clone -b phoenix https://gitlab.com/vprodus/docker-phoenix ~/projects/new_project```

Create docker volume on host  
```docker volume create --driver local pgdata```

To start your Phoenix server:

  * Navigate the to where you cloned this repository `cd ~/projects/new_project`

  * Start new project  
```./mix phx.new . --app new_project```  
or  
```./mix phx.new . --app new_project --live```  
The local `src/` directory is mapped inside the docker container as `/app`. Aanswer Y `/app` directory already exists.

  * Modify the Ecto configuration `src/config/dev.exs` to point to the DB container:
```
  # Configure your database
    config :new_project, NewProject.Repo,
    username: "postgres",
    password: "postgres",
    database: "new_project_dev",
    hostname: "db",
    show_sensitive_data_on_connection_error: true,
    pool_size: 10
```
  
  or if you want to use env variables that have been set in docker-compose.yml
```
  # Configure your database
    config :new_project, NewProject.Repo,
    username: System.get_env("PGUSER"),
    password: System.get_env("PGPASSWORD"),
    database: System.get_env("PGDATABASE"),
    hostname: System.get_env("PGHOST"),
    show_sensitive_data_on_connection_error: true,
    pool_size: 10
```
  
  
  * Initialize the Database with Ecto  
```./mix ecto.create```

  * Starting the Application  
```docker-compose up```

  * Run your database migration  
```./mix ecto.migrate```

  * Run commands other than mix tasks, use the ./run script  
```./run iex -S mix phx.serverr```

  * Run npm tasks  
```./run npm install --prefix assets```
